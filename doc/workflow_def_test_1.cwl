#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: Workflow
doc: |
    This is a very simple spectroscopy workflow of six steps.
    It will 1. Normalize spectrum
            2. post edge substraction (EXAFS)
            3. k selection
            4. fourier transform

requirements:
    - class: InlineJavascriptRequirement

inputs:
    input_data:
        type: File
    working_dir:
        type: Directory
outputs:
    output_ft:
        type: File

steps:
    normalization:
        in:
            input_data: input_data
            working_dir: working_dir
        out: [output_normalization]
        run:
            class: CommandLineTool
            baseCommand: ["python", "-m", "est.core.process.pymca.normalization"]
            inputs:
                input_data:
                    type: File
                    inputBinding:
                        position: 1
                working_dir:
                    type: Directory
                    inputBinding:
                        position: 2
            outputs:
                output_normalization:
                    type: File
                    outputBinding:
                        glob: output_normalization.yaml
    exafs:
        in:
            input_data: normalization/output_normalization
            working_dir: working_dir
        out: [output_exafs]
        run:
            class: CommandLineTool
            baseCommand: ["python", "-m", "est.core.process.pymca.exafs"]
            inputs:
                input_data:
                    type: File
                    inputBinding:
                        position: 1
                working_dir:
                    type: Directory
                    inputBinding:
                        position: 2
            outputs:
                output_exafs:
                    type: File
                    outputBinding:
                        glob: output_exafs.yaml
    k-weight:
        in:
            input_data: exafs/output_exafs
            working_dir: working_dir
        out: [output_k_weight]
        run:
            class: CommandLineTool
            baseCommand: ["python", "-m", "est.core.process.pymca.k_weight"]
            inputs:
                input_data:
                    type: File
                    inputBinding:
                        position: 1
            outputs:
                output_k_weight:
                    type: File
                    outputBinding:
                        glob: output_k_weight.yaml
    ft:
        in:
            input_data: k-weight/output_k_weight
            working_dir: working_dir
        out: [output_ft]
        run:
            class: CommandLineTool
            baseCommand: ["python", "-m", "est.core.process.pymca.ft"]
            inputs:
                input_data:
                    type: File
                    inputBinding:
                        position: 1
            outputs:
                output_ft:
                    type: File
                    outputBinding:
                        glob: output_ft.yaml
